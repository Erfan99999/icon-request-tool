* Corregido el fallo de funcionamiento en nuevas versiones de Android y un poco de limpieza de código.
Arreglados los nombres del fichero de iconos y se añade la opción de grabar el archivo en el almacenamiento local.
Añadida la opción de filtrar por iconos incluidos en el futuro.
Añadida la opción de enviar iconos actualizados. 
Actualizada la IU y mensajes. (by dkanada)
* Actualizaciones de compatibilidad, optimizaciones, y mantenimiento (gracias a TacoTheDank).
